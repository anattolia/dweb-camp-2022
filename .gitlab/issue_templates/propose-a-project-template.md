**To propose a project at DWeb Camp 2022 please use this template and open an issue or fill [the form](https://docs.google.com/forms/d/e/1FAIpQLSfzhhPhcoiVQ_-Iltmwx34XihhfuugCeaNb-ILMWcTLw0wahg/viewform)**

----------

## Topic


## Description
Please describe the nature of your session activity


## Hopes & dreams


## Core facilitators


## Main contact (email)


## Space Requirements
- [] Electricity
- [] Monitor
- [] Whiteboard or pad
- [] Table and chairs
- [] Seating in circle
- [] Open space outdoors
- [] Either indoors or outdoors is fine
- [] Other (Specify)

## Time Requirements
- [] 1 hour
- [] 2 hours
- [] Multiple sessions across 1 day
- [] Multiple sessions across 2 days 
- [] Multiple sessions across 3 days 
- [] Pre breakfast session
- [] Evening session
- [] Other (Specify)

## Who should participate
- [] Coders
- [] Creatives
- [] Policymakers
- [] Children under 12
- [] Teens 12 - 18
- [] Anyone and everyone
- [] Other (Specify)

## What will participants take away?


## Keywords


## Session rundown (tell us a rough outline of your plan)


## Relevant links
Relevant links about your project (optional)


## Relevant Experience -- share why you'll be great!


## Graphic or video link
Optional graphic or video to pitch your exciting project!



